# frozen_string_literal: true

module Constraints
  class Auth
    # Request has logged in user
    #
    # @param [Object] request
    # @return [Boolean]
    def matches?(request)
      return true if AppConfig.anonymous_access

      user_id = request.session[:current_user_id]
      return false unless user_id
      return true if find_user(user_id)

      false
    end

    private

    # Find user
    #
    # @param [Integer] id
    # @return [User]
    def find_user(id)
      User.find_by(id: id)
    rescue Mongoid::Errors::DocumentNotFound
      nil
    end
  end
end
