# frozen_string_literal: true

FactoryBot.define do
  factory :update_run, class: "Update::Run" do
    job { create(:update_job) }
  end
end
