# frozen_string_literal: true

describe LogEntryExpiryUpdater, :integration do
  before do
    Update::LogEntry.remove_indexes
    Update::LogEntry.create_indexes

    create(:update_log_entry)

    allow(AppConfig).to receive(:expire_run_data).and_return(3600)
  end

  it "updates ttl index for log entries model" do
    described_class.call

    expect(Update::LogEntry.collection.indexes.get("created_at_1")["expireAfterSeconds"].to_i).to eq(3600)
  end
end
