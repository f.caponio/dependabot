# What is dependabot-gitlab?

It is an app for automatically managing dependency updates.

`dependabot-gitlab` uses [dependabot-core](https://github.com/dependabot/dependabot-core) for dependency update logic and adds additional functionality to integrate these updates with GitLab.

::: warning
dependabot-gitlab is not affiliated with, funded by, or associated with the Dependabot team or GitHub
:::

::: warning
dependabot-gitlab is currently in alpha status. It is already suitable for out-of-the-box dependency updates, but the config options and api might still have breaking changes between minor releases
:::

## Distribution

Application is packaged as a docker image and is available from following registries:

- [Dockerhub](https://hub.docker.com/r/andrcuns/dependabot-gitlab/tags) - `docker.io/andrcuns/dependabot-gitlab:latest`
- [GitLab](https://gitlab.com/dependabot-gitlab/dependabot/container_registry/1286335) - `registry.gitlab.com/dependabot-gitlab/dependabot:latest`
