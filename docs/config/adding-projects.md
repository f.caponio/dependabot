# Adding projects

In order for application to start updating dependencies, projects have to be registered first which will create scheduled dependency update jobs. Several ways of adding projects exist.

Configuration options related to project registration can be found in [project registration](./environment.md#project-registration) section.

## Automatically

### Project registration job

It is possible to enable project registration job, which will periodically scan for projects to register.

This job will automatically add projects that have valid configuration files as well as sync outdated configurations or schedule cron expressions.

Gitlab access token configured for the application must have at least a maintainer access for a project to be automatically added.

### System webhook

If `SETTINGS__PROJECT_REGISTRATION` is set to `system_hook`, endpoint `api/project/registration` endpoint is enabled which listens for following [system hook](https://docs.gitlab.com/ee/system_hooks/system_hooks.html) events to automatically register projects:

- `project_create`
- `project_destroy`
- `project_rename`
- `project_transfer`

## Manually

## UI

Projects can be added via `add project` form from the main page

### API

Project can be added using [add project](../using/api.md#add-project) api endpoint

### Rake task

Projects can be added using [register](../using/rake.md#register-single-project) rake task
