# Merge request commands

::: warning
Not applicable for standalone mode
:::

Application supports several merge request commands. This feature requires [webhooks](../config/webhooks.md) to be configured.

Command prefix is configurable via [SETTINGS__COMMANDS_PREFIX](../config/environment.md#application) environment variable.

## Rebase

Comment `dependabot rebase`, will perform merge request rebase. This command does essentially the same action as GitLab built in `/rebase` command.

## Recreate

Comment `dependabot recreate` will recreate the merge request resolving all merge conflicts that might be present.

::: warning
This action will remove all manually added changes
:::
