# Security updates

::: warning
Not applicable for standalone mode
:::

Application supports syncing with [GitHub Advisory Database](https://github.com/advisories) for security vulnerability data retrieval when performing dependency updates.

This feature requires for [github access token](../config/environment.md#access) to be configured.

## Vulnerability alerts

When `dependabot-gitlab` detects security vulnerability in a dependency but is unable to update it, it will create security vulnerability issue instead.

It is possible to configure assignee for vulnerability issue via [vulnerability-alert](../config/configuration.md#vulnerability-alerts) configuration option.
