# Closing outdated merge requests

::: warning
Not applicable for standalone mode
:::

When a newer version update merge request is created for specific dependency, application will automatically close previous update merge requests. No additional configuration is required.
